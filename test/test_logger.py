import sys

sys.path.append("..")
import time
import swagger_client
from swagger_client.rest import ApiException
from src import Logger
import pytest

test_id1 = "2CF14477A6490CBF63C53A050E9C338518848A29-3"
test_id2 = "2CF14477A6490CBF63C53A050E9C338518848A29-4"
test_id3 = "2CF14477A6490CBF63C53A050E9C338518848A29-6"
token = "BUSD"
address = "bnb19nc5gaaxfyxt7c798gzsa8pns5vgfz3fx2c6kx"
tokens = ['BNB', 'RUNE', 'BUSD']


class TestLogger:
    def test_initialization(self):
        tester = Logger(tokens, address)
        assert (tester.balances[tokens[0]] > 0)
        assert (tester.balances[tokens[1]] > 0)
        assert (tester.balances[tokens[2]] > 0)

    def test_log_new_trade(self):
        tester = Logger(tokens, address)
        fullyfill = tester.log_new_trade(test_id1)
        assert (fullyfill == 1)
        fullyfill = tester.log_new_trade(test_id2)
        assert (fullyfill == 1)
        fullyfill = tester.log_new_trade(test_id3)
        assert (fullyfill == 1)

    def test_log_pnl(self):
        tester = Logger(tokens, address)
        tester.balances['BUSD'] = float(30)
        pnl = tester.log_pnl(tokens[2])
        assert (pnl > 0)
