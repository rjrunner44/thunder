import sys
import json
import os
import pytest
sys.path.append("..")
from src import Monitor, LiquidityError


class TestMonitor:
    """ Test on input that results in a negative profit """

    def test_invalid_trade_condition(self):
        pair1, pair2, pair3 = {}, {}, {}
        with open('trade_condition_suite/bnb_busd.json') as f:
            pair1 = json.load(f)
        with open('trade_condition_suite/rune_bnb.json') as f:
            pair2 = json.load(f)
        with open('trade_condition_suite/rune_busd.json') as f:
            pair3 = json.load(f)
        wallet_env = ''
        tester = Monitor(wallet_env, 1, 0.0004, 2)
        profit = tester.test_scan(pair1, pair2, pair3)
        assert (tester.alpha_msg._price == 16.299)
        assert (tester.alpha_msg._quantity == 0.061)
        assert (tester.beta_msg._price == 0.0208864)
        assert (tester.beta_msg._quantity == 2.9)
        assert (tester.theta_msg._price == 0.255002)
        assert (tester.theta_msg._quantity == 2.9)
        assert (profit < 0)

    ''' Test on input that results in a positive profit '''

    def test_valid_trade_condition_1(self):
        pair1, pair2, pair3 = {}, {}, {}
        with open('trade_condition_suite/bnb_busd1.json') as f:
            pair1 = json.load(f)
        with open('trade_condition_suite/rune_bnb.json') as f:
            pair2 = json.load(f)
        with open('trade_condition_suite/rune_busd.json') as f:
            pair3 = json.load(f)
        wallet_env = ''
        tester = Monitor(wallet_env, 1, 0.0004, 2)
        profit = tester.test_scan(pair1, pair2, pair3)
        assert (tester.alpha_msg._price == 11.09159)
        assert (tester.alpha_msg._quantity == round(0.09016000, tester.alpha_lot_size))
        assert (tester.beta_msg._price == 0.0208864)
        assert (tester.beta_msg._quantity == round(4.31668000, tester.beta_lot_size))
        assert (tester.theta_msg._price == 0.255002)
        assert (tester.theta_msg._quantity == round(4.31668000, tester.beta_lot_size))
        assert (profit > 0.05)

    def test_valid_trade_condition_2(self):
        pair1, pair2, pair3 = {}, {}, {}
        with open('trade_condition_suite/bnb_busd.json') as f:
            pair1 = json.load(f)
        with open('trade_condition_suite/rune_bnb1.json') as f:
            pair2 = json.load(f)
        with open('trade_condition_suite/rune_busd.json') as f:
            pair3 = json.load(f)
        wallet_env = ''
        tester = Monitor(wallet_env, 1, 0.0004, 2)
        profit = tester.test_scan(pair1, pair2, pair3)
        assert (tester.alpha_msg._price == 16.299)
        assert (tester.alpha_msg._quantity == round(0.06135000, tester.alpha_lot_size))
        assert (tester.beta_msg._price == 0.014222)
        assert (tester.beta_msg._quantity == round(4.31374000, tester.beta_lot_size))
        assert (tester.theta_msg._price == 0.255002)
        assert (tester.theta_msg._quantity == round(4.31374000, tester.beta_lot_size))
        assert (profit > 0.05)

    def test_valid_trade_condition_3(self):
        pair1, pair2, pair3 = {}, {}, {}
        with open('trade_condition_suite/bnb_busd.json') as f:
            pair1 = json.load(f)
        with open('trade_condition_suite/rune_bnb.json') as f:
            pair2 = json.load(f)
        with open('trade_condition_suite/rune_busd1.json') as f:
            pair3 = json.load(f)
        wallet_env = ''
        tester = Monitor(wallet_env, 1, 0.0004, 2)
        profit = tester.test_scan(pair1, pair2, pair3)
        assert (tester.alpha_msg._price == 16.299)
        assert (tester.alpha_msg._quantity == round(0.06135, tester.alpha_lot_size))
        assert (tester.beta_msg._price == 0.0208864)
        assert (tester.beta_msg._quantity == round(2.93732, tester.beta_lot_size))
        assert (tester.theta_msg._price == 0.3607)
        assert (tester.theta_msg._quantity == round(2.93732, tester.beta_lot_size))
        assert (profit > 0.03)

    ''' Test on input that results in breakeven without considering fee '''

    def test_breakeven_trade_condition(self):
        pair1, pair2, pair3 = {}, {}, {}
        with open('trade_condition_suite/bnb_busd.json') as f:
            pair1 = json.load(f)
        with open('trade_condition_suite/rune_bnb.json') as f:
            pair2 = json.load(f)
        with open('trade_condition_suite/rune_busd2.json') as f:
            pair3 = json.load(f)
        wallet_env = ''
        tester = Monitor(wallet_env, 1, 0.0004, 2)
        profit = tester.test_scan(pair1, pair2, pair3)
        assert (tester.alpha_msg._price == 16.299)
        assert (tester.alpha_msg._quantity == round(0.06135, tester.alpha_lot_size))
        assert (tester.beta_msg._price == 0.0208864)
        assert (tester.beta_msg._quantity == round(2.93732, tester.beta_lot_size))
        assert (tester.theta_msg._price == 0.3407)
        assert (tester.theta_msg._quantity == round(2.93732, tester.beta_lot_size))
        assert (profit < 0)

    ''' Test on input that results in positive profit but not enough volume '''

    def test_liquidity(self):
        pair1, pair2, pair3 = {}, {}, {}
        with open('liquidity_suite/bnb_busd.json') as f:
            pair1 = json.load(f)
        with open('liquidity_suite/rune_bnb.json') as f:
            pair2 = json.load(f)
        with open('liquidity_suite/rune_busd.json') as f:
            pair3 = json.load(f)
        wallet_env = ''
        tester = Monitor(wallet_env, 1, 0.0004, 2)
        with pytest.raises(LiquidityError, match="BNB/BUSD"):
            assert (tester.test_scan(pair1, pair2, pair3))
        with open('liquidity_suite/bnb_busd1.json') as f:
            pair1 = json.load(f)
        with pytest.raises(LiquidityError, match="RUNE/BNB"):
            assert (tester.test_scan(pair1, pair2, pair3))
        with open('liquidity_suite/rune_bnb1.json') as f:
            pair2 = json.load(f)
        with pytest.raises(LiquidityError, match="RUNE/BUSD"):
            assert (tester.test_scan(pair1, pair2, pair3))
