import sys

sys.path.append("..")
import time
import swagger_client
import argparse
from swagger_client.rest import ApiException
from binance_chain.http import HttpApiClient
from binance_chain.messages import NewOrderMsg
from binance_chain.wallet import Wallet
from binance_chain.environment import BinanceEnvironment
from src import Monitor, LiquidityError
from src import Logger


class Hunter:
    def __init__(self, key):
        self.key = key
        self.env = BinanceEnvironment()
        self.wallet = Wallet(private_key=key, env=self.env)
        self.client = HttpApiClient(env=self.env)
        self.tokens = ['BNB', 'RUNE', 'BUSD']
        print("Wallet initialized: ")
        print("address: " + self.wallet.address)

    def hunt(self):
        base_size = 1
        fee = 0.0004
        omega = 2
        token = "BUSD"
        rate = 1
        monitor = Monitor(self.wallet, base_size, fee, omega)
        logger = Logger(self.tokens, self.wallet.address)
        while True:
            try:
                profit = monitor.scan(token)
                print(profit)
                if profit > 0:
                    print(profit)
                    print("executing alpha")
                    res_alpha = self.client.broadcast_msg(monitor.alpha_msg, sync=True)
                    id_alpha = res_alpha[0]['data'][13:-2]
                    bool_alpha = logger.log_new_trade(id_alpha)
                    if bool_alpha:
                        # check if alpha succeeded
                        print("executing beta")
                        res_beta = self.client.broadcast_msg(monitor.beta_msg, sync=True)
                        id_beta = res_alpha[0]['data'][13:-2]
                        bool_beta = logger.log_new_trade(id_beta)
                        if bool_beta:
                            # check if beta succeeded
                            print("executing theta")
                            res_theta = self.client.broadcast_msg(monitor.theta_msg, sync=True)
                            id_theta = res_alpha[0]['data'][13:-2]
                            bool_theta = logger.log_new_trade(id_theta)
                            if bool_theta:
                                logger.log_pnl(self.tokens[2])
                    # send orderIds to sQL
                time.sleep(1/rate)
            except LiquidityError as e:
                print("Liquidity Issue on pair %s\n" % e)


